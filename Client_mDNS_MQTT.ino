#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <PubSubClient.h>
#include <WiFiManager.h>
#include <String.h>
#include<DHT.h>

//Declaration your WiFi here.
const char* ssid = "MakerHanoi-2-1";
const char* password = "makerhanoi@12345621";

//MQTT channel configuration
//const char* mqtt_server = "192.168.1.76";
const char* device_id = "NodeMcuEsp01";  // uuid
const char* channel_in_postfix = "/in";
const char* channel_out_postfix = "/out";
String channel_in = "esp/", channel_out = "esp/";

//const char* outTopic = "ESP8266/out";
//const char* inTopic = "ESP8266/LED status";

// Initialize DHT sensor
const int DHTPIN = A0;       //Đọc dữ liệu từ DHT11 ở chân A0 trên ESP
const int DHTTYPE = DHT11;  //Khai báo loại cảm biến, có 2 loại là DHT11 và DHT22
DHT dht(DHTPIN, DHTTYPE); // 11 works fine for ESP8266

float humidity, temp_c;   // Values read from sensor
// Generally, you should use "unsigned long" for variables that hold time
unsigned long previousMillis = 0;        // will store last temp was read
const long cnt_sensor = 2000;              // interval at which to read sensor - See more at: http://www.esp8266.com/viewtopic.php?f=29&t=8746#sthash.IJ0JNSIx.dpuf
unsigned long preTimer = 0;
const long interval = 5000;             // interval to publish data each time.

WiFiClient espClient;
PubSubClient client(espClient);
char hostString[16] = {0};
char IP_Server_char[20];
int Port_Server;

void setup() {
  channel_in += device_id;          // "esp/NodeMcuEsp01"
  channel_in += channel_in_postfix; // "esp/NodeMcuEsp01/in"
  Serial.println("Subscribe channel: ");
  Serial.println(channel_in);

  channel_out += device_id;           // "esp/NodeMcuEsp01"
  channel_out += channel_out_postfix; // "esp/NodeMcuEsp01/out"
  Serial.println("Channel out: ");
  Serial.println(channel_out);

  pinMode(D0,OUTPUT);
  pinMode(D1,OUTPUT);
  pinMode(D2,OUTPUT);
  pinMode(D3,OUTPUT);
  pinMode(D4,OUTPUT);
  
  pinMode(D5,INPUT);
  pinMode(D6,INPUT);
  pinMode(D7,INPUT);
  pinMode(D8,INPUT);
  pinMode(A0,INPUT);
  
  Serial.begin(115200);
  setup_wifi();

}

void setup_wifi(){
  Serial.begin(115200);
  delay(100);

  sprintf(hostString, "ESP_%06X", ESP.getChipId());
  Serial.print("Hostname: ");
  Serial.println(hostString);
  WiFi.hostname(hostString);
  
  // We start by connecting to a WiFi network
   WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connecting to ");
  Serial.println(ssid);
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
/*
}

void setup_mDNS_server(){ */
    if (!MDNS.begin(hostString)) {
    Serial.println("Error setting up MDNS responder!");
  }
//  Serial.println("mDNS responder started");
//  MDNS.addService("http", "tcp", 80); // Announce esp tcp service on port 8080

  Serial.println("Sending mDNS query");
  int n = MDNS.queryService("mqtt", "tcp"); // Send out query for esp tcp services ----------- finding server
  Serial.println("mDNS query done");
  if (n == 0) {
    Serial.println("no services found");
  }
  else {
    Serial.println("Service found");
    Serial.println("Host: " + String(MDNS.hostname(0)));
    Serial.print("IP  : " );
    Serial.println(MDNS.IP(0));
    Serial.println("Port: " + String(MDNS.port(0)));
  }

  ///////////////// Get IP Adress and Port Adress/////////////
  IPAddress ip = MDNS.IP(0); // Get IPAddress of Server
  String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]); // convert IP Adress to String
  ipStr.toCharArray(IP_Server_char, 20);  // convert IP to char
  Serial.println(IP_Server_char);

  Port_Server = MDNS.port(0);
//  Serial.println(IP_Server_char);
//  Serial.println(Port_Server);
  //  client.setServer(mqtt_server, 1883);//////////////////////////
  client.setServer(IP_Server_char,Port_Server); // connect to Server after get succefully IP Adress and Port..
  client.setCallback(callback);
  reconnect();
}

  
void callback(char* topic, byte* payload, unsigned int length) {
  payload[length] = '\0'; // Null terminator used to terminate the char array
  String message = (char*)payload;
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
/*  for (int i = 0; i < length; i++) {  // print message converted from byte to char
    Serial.print((char)payload[i]);
  }
*/
  char a[32];
  char b[50]; 
  message.toCharArray(b,50);
  
  strncpy(a, b, length - 11);
//  Serial.println(a);
  Serial.println(" a converted : ");
  unsigned char **A = Convert(a);
  for(int i =0; i< 5; i++)
  {
    Serial.print(A[i][0]);
    Serial.print("  ");
    Serial.println(A[i][1]);     

    if(A[i][0] == 0)
    {
      switch(i)
      {
        case 0: 
          if(A[i][1] == 0)
            digitalWrite(D0,LOW);
           else
            digitalWrite(D0,HIGH); break;
        case 1: 
          if(A[i][1] == 0)
            digitalWrite(D1,LOW);
           else
            digitalWrite(D1,HIGH); break;
        case 2: 
            if(A[i][1] == 0)
              digitalWrite(D2,LOW);
             else
              digitalWrite(D2,HIGH); break;
        case 3: 
          if(A[i][1] == 0)
            digitalWrite(D3,LOW); 
           else
            digitalWrite(D3,HIGH); break;
        case 4: 
          if(A[i][1] == 0)
            digitalWrite(D4,LOW);
           else
            digitalWrite(D4,HIGH); break;
    }   
   }
   else
   {
    switch(i)
    {
      case 0: analogWrite(D0,A[i][1]); break;
      case 1: analogWrite(D1,A[i][1]); break;
      case 2: analogWrite(D2,A[i][1]); break;
      case 3: analogWrite(D3,A[i][1]); break;
      case 4: analogWrite(D4,A[i][1]); break;
    }
   }
  }

  int j = length;
//  Serial.print("length : ");
//  Serial.print(b);
//  Serial.println(j);
  if(b[j-3] == '1')
    Function_F5();
  if(b[j-5] == '1')
    Function_F4();
  if(b[j-7] == '1')
    Function_F3();
  if(b[j-9] == '1')
    Function_F2();
  if(b[j-11] == '1')
    Function_F1();

/*
  Serial.println(message);
//  if((char)payload[0] == 'o' && (char)payload[1] == 'n') //on
  if(!message.indexOf("on"))
    digitalWrite(D4,LOW);
//  else if((char)payload[0] == 'o' && (char)payload[1] == 'f' && (char)payload[2] == 'f') //off
   else if(!message.indexOf("off"))
    digitalWrite(D4,HIGH);
*/
}

///////////////// Convert data payload for D0-D4///////////////
unsigned char** Convert(char* str)
{
  int count;
  char* p = str;
  count = 1;
  int index;

    unsigned char **store = new unsigned char*[5];
    index = 0;
    for(int i = 0; i < 5; i++)
  {
    store[i] = new unsigned char[2];
    store[i][0] = str[index] - 48;
    index += 2;
    char ch = 0;
    while(1)
    {
      if(str[index] == ';' || str[index] == 0)
      {
        index++;
        break;
      }
      ch = 10 * ch + (str[index++] - 48);
    }
    store[i][1] = ch;
  }
  return store;
}


//////////////////////////////PUB/////////////////
void send_data(){
  int D5=digitalRead(D5);
  int D6=digitalRead(D5);
  int D7=digitalRead(D5);
  int D8=digitalRead(D5);
  int A0=analogRead(D5);
  int U1= Virtual_U1();
  int U2= Virtual_U2();
  int U3= Virtual_U3();
  int U4= Virtual_U4();
  int U5= Virtual_U5();
  String pubString = String(D5) + ';' + String(D6)+ ';' + String(D7) + ';' + String(D8) + ';' + String(A0) + ';' + String(U1) + ';' + String(U2) + ';' + String(U3) + ';' + String(U4) + ';' + String(U5) ;
  char pubChar[50];
  pubString.toCharArray(pubChar,50);

  unsigned long curTimer = millis();
 if(curTimer - preTimer >= interval)   // set time cycle to publish data: each 5s
 {
  preTimer = curTimer;
  client.publish(channel_out.c_str(),pubChar);
  Serial.println(pubChar);
 }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
//    if (client.connect("ESP8266Client")) {
    if (client.connect(device_id)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
//      client.publish("ESP8266/connection status", "Connected!");
//      client.publish(channel_out.c_str(), device_id);
      // ... and resubscribe
//      client.subscribe("ESP8266/LED status");
      client.subscribe(channel_in.c_str());
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void gettemperature() {
  // Wait at least 2 seconds seconds between measurements.
  // if the difference between the current time and last time you read
  // the sensor is bigger than the interval you set, read the sensor
  // Works better than delay for things happening elsewhere also
  unsigned long currentMillis = millis();
 
  if(currentMillis - previousMillis >= cnt_sensor) {
    // save the last time you read the sensor 
    previousMillis = currentMillis;   

    // Reading temperature for humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (it's a very slow sensor)
    humidity = dht.readHumidity();          // Read humidity (percent)
    temp_c = dht.readTemperature();     // Read temperature as Celcius
    // Check if any reads failed and exit early (to try again).
/*    if (isnan(humidity) || isnan(temp_c)) {
      Serial.println("Failed to read from DHT sensor!");
      return;
    }*/
  }
}


//////////////////////// Virtual output//////////////////
inline unsigned char Virtual_U1(){
  if(temp_c >=35)
    return 1;
   return 0;
  }

inline unsigned char Virtual_U2(){
  return 1;
}

inline unsigned char Virtual_U3(){
  return 1;
}

inline unsigned char Virtual_U4(){
  return 1;
}

inline unsigned char Virtual_U5(){
  return 1;
}

/////////////////////////////// Function input/////////////////////
inline void Function_F1(){
  Serial.println("F1");
  }

inline void Function_F2(){
  Serial.println("F2");
}

inline void Function_F3(){
  Serial.println("F3");
}
inline void Function_F4(){
  Serial.println("F4");
}

inline void Function_F5(){
  Serial.println("F5");
}


void loop() {
 
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  send_data();
}
